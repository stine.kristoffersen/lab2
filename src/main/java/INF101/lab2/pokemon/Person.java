package INF101.lab2.pokemon;

// Lager her en classe kalt for person hvor vi legger til navn og alder
// Denne klassen hentes ut igjen i prøvogfeil.java

public class Person {
    private String name;
    private int age;

    public Person(String name, int age) { // Konstruktør
        this.name = name;
        this.age = age;
        
    } 

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        if (age < 0) {
            System.err.println("Age is unvalid");
        }
        this.age = age;
    }
}
