package INF101.lab2.pokemon;

public class Main {

    public static Pokemon pokemon1;
    public static Pokemon pokemon2;
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated

        pokemon1 = new Pokemon("Cool", 30, 5);
        pokemon2 = new Pokemon("Petter", 50, 10);

        System.out.println(pokemon1);
        System.out.println(pokemon2);

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);

            pokemon2.attack(pokemon1);

        }
    }
}
